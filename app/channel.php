<?php

$GLOBALS["json_obj"] = getcwd().'/archive/array.json';
if(
    isset($_POST["name"]) &&
    isset($_POST["channel"])&&
    isset($_POST["category"])&&
    isset($_POST["about"])&&
    isset($_POST["country"])
  )
{
if(
    ($name=$_POST["name"]) &&
    ($channel=$_POST["channel"])&&
    ($category=$_POST["category"])&&
    ($about=$_POST["about"])&&
    ($country=$_POST["country"])
  ){
    $name = strtolower($name);
    
    Channels::createChannel($name,str_replace(array("\n", "\r"), ' ', trim($channel)),null,$category,str_replace(array("\n", "\r"), ' ', trim($about)),$country);
    Channels::upload($name);
    Channels::buildObj(); 
    
}

}

if(isset($_GET["ch"]))
switch($_GET["ch"]){
     case "ky": Channels::getKeys(); break;    
     case "all": Channels::buildObj(); break;    
     case "del": if(isset($_POST["name"]) && isset($_POST["id"])){Channels::delObj($_POST["name"],$_POST["id"]);} break;    
     case "up": if(isset($_POST["name"]) && isset($_POST["channel"])){Channels::updateObj($_POST["name"],str_replace(array("\n", "\r"), ' ', trim($_POST["channel"])));} break;    
     default:  break;    
}




//echo json_encode(json_decode(file_get_contents($GLOBALS["json_obj"]), false));


class Channels{

    public static $channels = array();
    
    public static function createChannel($name=null,$channel=null,$img=null,$category=null,$about=null,$country=null){
        
        self::$channels = json_decode(file_get_contents($GLOBALS["json_obj"]), true);
        
        if(!array_key_exists($name,self::$channels)){
        
             $ch = new Channel($name);            
       
             $ch->addChannel($channel);
             $ch->img=$img;
             $ch->category=$category;
             $ch->about=$about;
             $ch->country=$country;       
             self::$channels[$name]=$ch->getChannel();
          
         }  else {
          
            self::$channels[$name]["url"]= $img;
            self::$channels[$name]["category"] = $category;
            self::$channels[$name]["about"] = $about;
            self::$channels[$name]["country"] = $country; 
                
                
           
            $alreadyExist=false;
            foreach(self::$channels[$name]["channels"] as $c){
              
                  preg_match('#<iframe(.*?)></iframe>#is', $channel, $match1);
                  preg_match('#<iframe(.*?)></iframe>#is', $c, $match2);
               
                  if($match1[1] == $match2[1]){
                     $alreadyExist=true;
                  }              
            }
          
            if(!$alreadyExist){                
                self::$channels[$name]["channels"][]=$channel;
            }
            
        }
        
    }
    
    
    public static function upload($name){
        $target_dir = getcwd()."/img/";
        $target_file = $target_dir . $name .'.'. explode(".",basename($_FILES["fileToUpload"]["name"]))[1];
        $target_file2 = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file2,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        //if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
               
                $uploadOk = 1;
            } else {
                
                $uploadOk = 0;
            }
       // }
        // Check if file already exists
        /*if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }*/
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                self::$channels[$name]["url"]=$target_file;
                file_put_contents($GLOBALS["json_obj"],json_encode(self::$channels));
   
               
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }

    }
    
    public static function getSrc($videoEmbed){
        $doc = new DOMDocument();
        $doc->loadHTML($videoEmbed);
        $src = $doc->getElementsByTagName('iframe')->item(0)->getAttribute('src');
        return $src;
    }
    
    public static function delObj($name, $index){
        self::$channels = json_decode(file_get_contents($GLOBALS["json_obj"]), true);
        $isUpdate=false;
        if(array_key_exists($name,self::$channels)){        
                         
              unset(self::$channels[$name]["channels"][$index]);
              $isUpdate = array_key_exists($index,self::$channels[$name]["channels"]);
                  
        }  
        file_put_contents($GLOBALS["json_obj"],json_encode(self::$channels));               
        echo json_encode($isUpdate);
    }
    
    public static function updateObj($name,$channel){        
        
        self::$channels = json_decode(file_get_contents(), true);
        file_put_contents(getcwd()."/archive/array_".time().".json",json_encode(self::$channels)); 
        
        if(array_key_exists($name,self::$channels)){        
            $alreadyExist=false;
        
            foreach(self::$channels[$name]["channels"] as $c){
                
                if(substr(trim($channel), 0, 4)!="http")
                  if(self::getSrc($channel) == self::getSrc($c)){
                     $alreadyExist=true;
                  }              
            }
          
            if(!$alreadyExist){                
                self::$channels[$name]["channels"][]=$channel;
            }
          
        }  
        file_put_contents($GLOBALS["json_obj"],json_encode(self::$channels));               
        echo json_encode(self::$channels);
    } 
    
    
    public static function buildObj(){
        self::$channels = json_decode(file_get_contents($GLOBALS["json_obj"]), false);
        //print_r(self::$channels);
        echo json_encode(self::$channels);
    }  
    
    public static function getKeys(){
        self::$channels = json_decode(file_get_contents($GLOBALS["json_obj"]), true);
        echo json_encode(array_keys(self::$channels));
    }

}

class Channel{
    
    public $name;
    private $channels;
    public $img;
    public $category;
    public $about;
    public $country;
    
    
    public function __construct($name) {
        $this->channels=array();
        $this->name = $name;
        $this->img = "";
        $this->category = "";
        $this->about = "";
        $this->country="";
    }
    
    public function addChannel($ch){
        $this->channels[]=$ch;        
    }    
    
    public function getChannel(){        
        return array(
            "name"=> $this->name,
            "channels"=> $this->channels,
            "url"=> $this->img,
            "category"=> $this->category,
            "about"=> $this->about,
            "country"=> $this->country                    
                    );
    }
    
    
}



?>