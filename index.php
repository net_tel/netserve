<!DOCTYPE html>
<html ng-app="nettvApp" lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <title>NetTV</title>

    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,900,600|Pacifico' rel='stylesheet' type='text/css'>

</head>

<body ng-controller="nettvController">

    <div class="container">
        <div ng-view></div>
      
    </div>

    <script src="/static/js/angular.min.js"></script>
    <script src="/static/js/angular-route.js"></script>
    <script src="/static/js/jquery.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <script src="/static/js/util/app.js"></script>

</body>

</html>