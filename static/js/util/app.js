	// create the module and name it preMainApp
	var preMainApp = angular.module('nettvApp', ['ngRoute']);



	// configure our routes
	preMainApp.config(function ($routeProvider) {
	    $routeProvider

	        .when('', {
	            templateUrl: '/templates/home.html',
	            controller: 'nettvController'
	        }).when('/', {
	            templateUrl: '/templates/home.html',
	            controller: 'nettvController'
	        })
	        .otherwise({
	            redirectTo: '/'
	        });
	    // $locationProvider.html5Mode(true);
	});


	preMainApp.filter('rawHtml', function ($sce) {
	    return function (val) {
            if(val.substring(0, 4) === "http")
                return $sce.trustAsHtml('<iframe style="width:400px;height:400px;" src="'+val+'"></iframe>');
	        return $sce.trustAsHtml(val);
	    };
	});

	preMainApp.value('frame', {
	    name: '',
	    id: 0
	});

	preMainApp.factory('Request', function ($http, $q) {
	    return {
	        getRequest: function (url) {
	            var promise = $q.defer();
	            $http.get(url).success(function (data) {
	                promise.resolve(data);
	            }).error(function (data) {
	                promise.reject(data);
	            });
	            return promise.promise;
	        },

	        putRequest: function (url, postdata) {

	            var promise = $q.defer();
	            $http.put(url, postdata).success(function (data) {
	                promise.resolve(data);
	            }).error(function (data) {
	                promise.reject(data);
	            });
	            return promise.promise;
	        },

	        postRequest: function (url, postdata) {

	            //postdata=encodeURIComponent(JSON.stringify(postdata));

	            var promise = $q.defer();
	            $http.post(url, postdata, {
	                headers: {
	                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
	                }

	            }).then(function (data) {
	                promise.resolve(data);
	            }, function (data) {
	                promise.reject(data);
	            });
	            return promise.promise;

	        }
	    };
	});


	preMainApp.controller('nettvController', function ($scope, Request, frame, $routeParams) {

	    var chs = function () {
	        Request.getRequest("/channel?ch=all").then(function (data) {
	            data = data;
	            $scope.channels = data;

	            $scope.frame = '<iframe></iframe>';
	            $('#frameModal').on('show.bs.modal', function (e) {
	                var frame_id = $(e.relatedTarget).data('frame-id');
	                var name = $(e.relatedTarget).data('frame-name');

	                $scope.$apply(function () {
	                    $scope.frame = $scope.channels[name].channels[frame_id];
	                    $scope.frame_id = frame_id;
	                    $scope.frame_name = name;
	                });
	                console.log($scope.frame);
	            });

	        }, function (data) {
	            console.log(data);

	        });
	    }
	    chs();

	    var keys = function () {
	        Request.getRequest("/channel?ch=ky").then(function (data) {
	            data = data;
	            $scope.keys = data;

	            console.log($scope.keys);
	        }, function (data) {
	            console.log(data);

	        });
	    }
	    keys();



	    $('#frameModal').on('shown.bs.modal', function () {
	        frame.id = $('#frameModal .btn-danger').attr("val_id");
	        frame.name = $('#frameModal .btn-danger').attr("val_name");
	        //console.log(frame);
	    });

	    $('#delete').on('shown.bs.modal', function () {
	        $('#delete .btn-success').click(function () {
	            console.log(frame);
	            Request.postRequest("/channel?ch=del", $.param(frame)).then(function (data) {
	                $('#delete').modal('toggle');
	                chs();
	                console.log(data);
	            }, function (data) {
	                console.log(data);
	            });

	        });
	    });



	});


	preMainApp.controller('modalController', function ($scope, Request, $routeParams) {

	    $('#update').submit(function (e) {
	        e.preventDefault();
	        Request.postRequest("/channel?ch=up", $(this).serialize()).then(function (data) {
	            $('#updateModal').modal('toggle');
	            data = data.data;
	            $scope.channels = data;
	            console.log(data);
	        }, function (data) {
	            console.log(data);
	        });
	    });

	    $('#upload').submit(function (e) {
	        e.preventDefault();

	        var name = $("input[name='name']").val();
	        var channel = $("textarea[name='channel']").val();
	        var about = $("textarea[name='about']").val();
	        var category = $("input[name='category']").val();
	        var country = $("input[name='country']").val();

	        var file_data = $('#fileToUpload').prop('files')[0];

	        var form_data = new FormData();
	        form_data.append('fileToUpload', file_data);
	        form_data.append('name', name);
	        form_data.append('channel', channel);
	        form_data.append('about', about);
	        form_data.append('category', category);
	        form_data.append('country', country),

	            $.ajax({
	                url: '/channel', // point to server-side PHP script 
	                dataType: 'text', // what to expect back from the PHP script, if anything
	                cache: false,
	                contentType: false,
	                processData: false,
	                data: form_data,
	                type: 'post',
	                success: function (php_script_response) {
	                    Request.getRequest("/channel?ch=all").then(function (data) {
	                        $('#myModal').modal('toggle');
	                        data = data;
	                        $scope.channels = data;
	                        console.log(data);
	                    }, function (data) {
	                        console.log(data);
	                    });


	                    Request.getRequest("/channel?ch=ky").then(function (data) {
	                        data = data;
	                        $scope.keys = data;

	                        console.log($scope.keys);
	                    }, function (data) {
	                        console.log(data);

	                    });

	                }
	            });
	    });

	});

	preMainApp.directive('myModal', function () {
	    return {
	        templateUrl: '/templates/modal.html',
	        controller: 'modalController'
	    };
	});